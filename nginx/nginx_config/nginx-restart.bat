@ECHO OFF
ECHO Stopping nginx and PHP7
cd c:/nginx

call nginx-stop.bat
ECHO Validating nginx configuration
call nginx -t
ECHO Starting nginx
call nginx-start.bat

pause
