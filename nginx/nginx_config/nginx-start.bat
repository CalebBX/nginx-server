@ECHO OFF
pushd C:\nginx
cd c:\nginx

ECHO Starting PHP FastCGI...
RunHiddenConsole.exe "C:\Program Files (x86)\PHP7\php-cgi.exe" -b 127.0.0.1:9123 -c "C:\Program Files (x86)\PHP7\php.ini"

ECHO Starting NGINX
start nginx.exe

popd
EXIT /b
