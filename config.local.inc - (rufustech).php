<?php

	/*
	 * API config
	 * 
	 *      local version, copy to config.local.inc.php
	 */

	$config_data = array(

		// debugging
		"show_errors"         => true,
		"slim_show_debug"     => true,
		"debug_queries"       => false,
		"log_slim_enabled"    => true,
		"log_slim_level"      => "ERROR", // DEBUG, INFO, NOTICE, WARN, ERROR, CRITICAL, ALERT, EMERGENCY
		"log_php_errors"      => true,
		"firephp_enabled"     => true,

		// image uploaders
		//"image-uploader-optimize"   => false,
		//"image-uploader-get-color"  => false,

		// AJAX CORS authorization
		"ajaxCorsUrl"         => "http://domain.localhost:8080",

		// database
		"database"            => array(
			// MySQL
			"database" => "rufus",
			"type"     => "mysql",
			"host"     => "127.0.0.1",
			"user"     => "root",
			"password" => "password",
		),

		/*
		// SQLite
		"database" => dirname( __FILE__ ) . "/../db/cms.local.sqlite",
		"type"     => "sqlite",
		),
		*/

		// SMTP server
		"email"               => array(
			// HostDime SMTP
			"username"              => "",
			"password"              => "",
			"smtp"                  => true,
			"smtp_auth"             => true,
			"secure"                => true,
			"smtp_port"             => 465,
			"debugSmtp"             => false,
			"validate_certificates" => false,
			"host"                  => "",

			/*
			// Mailgun SMTP
				"host"                  => "smtp.mailgun.org",
				"username"              => "",
				"password"              => "",
				"smtp"                  => true,
				"smtp_auth"             => true,
				"smtp_port"             => 465, // 25, 465
				"secure"                => true,
				"validate_certificates" => true,
				//"mailgun_api_url"=> "https://api.mailgun.net/v3/demo.simplimation.com",
				//"mailgun_api_key" => "",
	
			// Rackspace
				// use "ssl" method port 465
	
			// mail()
				"username"              => "demo@example.com",
				"password"              => "",
				"smtp"                  => false,
				"smtp_auth"             => false,
				"smtp_port"             => false, // 25, 465
				"secure"                => false,
				"validate_certificates" => false,
				"host"                  => "",
	
			// GoDaddy SMTP
				"smtp"      => true,
				"host"      => 'relay-hosting.secureserver.net',
				"smtp_port" => 25,
				"smtp_auth" => false,
				"secure"    => false,
			*/
		),

		// admin database access key
		// required to browse to db setup routes: http://rufustech.localhost:8080/api/db/reset?admin_db_access_key=[key]
		"admin_db_access_key" => "1c8pp4cgjycbkmohzgdo",

		// Slim encrypted cookies
		'encryption_key'      => "Vue-CMS___c)a&l@e%b*i1s*a;w'e_s+o*m!e",

		// JWT token encryption secret
		"jwt_secret_key"      => "JWTSECRETKEY",

		// cron
		"cron_key"            => "CRONKEY",

		// testing
		"login_passwordless_skip_email" => false,
	);
